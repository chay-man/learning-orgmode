#+title:  LinkedIn Statistics Foundations 1 Notes
#+author: Antonio Chay
#+date:  <2023-01-04 Wed>
#+STYLE: <SCRIPT SRC="/usr/share/jsmath/easy/load.js"></SCRIPT>
#+STARTUP: latexpreview

* Data and Charts

Frequency : Count the repeating events (sort|uniq -c).


Histogram: Display the frequency of the events.

* The Middle

Mean: average
Median: sort and find the middle \( M = \frac{n+1}{2} \)
Mode: most common number.
